const { Collection } = require ('postman-collection')
const items = require('./items')

const filename = './collection/collection.json'

const postmanCollection = new Collection({
    info: {
        name: "User Game Collection"
    },

    item: [],
});

// add auth requests to collection
postmanCollection.items.add(items.auth.requests.register);
postmanCollection.items.add(items.auth.requests.login);
postmanCollection.items.add(items.auth.requests.whoami);
postmanCollection.items.add(items.auth.requests.changePassword);

// add user requests to collection
postmanCollection.items.add(items.userGame.requests.index);
postmanCollection.items.add(items.userGame.requests.show);
postmanCollection.items.add(items.userGame.requests.update);
postmanCollection.items.add(items.userGame.requests.destroy);

// add user game biodata 
postmanCollection.items.add(items.userGameBiodata.requests.create);
postmanCollection.items.add(items.userGameBiodata.requests.show);
postmanCollection.items.add(items.userGameBiodata.requests.update);
postmanCollection.items.add(items.userGameBiodata.requests.destroy);

// add user game history 
postmanCollection.items.add(items.userGameHistory.requests.create);
postmanCollection.items.add(items.userGameHistory.requests.show);

const collectionJSON = postmanCollection.toJSON();

const fs = require('fs')

fs.writeFile(filename, JSON.stringify(collectionJSON), (err) => {
    if (err)  { console.log(err); }
    console.log(`File Saved at ${filename} `)
});
