const { Item } = require ('postman-collection');
const names = require('../names');
const endpoints = require('../endpoints');
const payloads = require('../payloads');
const headers = require('../headers');

// test if 200 then we can get the message of test successful kalau menggunakan jaz.
const requestTest = `
pm.test('Contoh Testing: Test for successfull response', function() {
    pm.expect(pm.response.code).to.equal(200);
});
`
const requests = { 
    create: new Item({
        name: names.userGameHistory.create,
        request: {
            header: headers.auth.requestHeader.showUserGameHistory,
            url: endpoints.userGameHistory.create,
            method: 'POST',
            body: {
                mode: 'raw',
                raw: JSON.stringify(payloads.userGameHistory.create)
            },
            auth: null
        }, 
        events: [
            {
                listen: 'test',
                scripts: {
                    type: 'text/javascript',
                    exec: requestTest
                }
            }
        ]
    }),

    show: new Item({
        name: names.userGameHistory.show,
        request: {
            header: headers.auth.requestHeader.showUserGameHistory,
            url: endpoints.userGameHistory.show,
            method: 'GET',
            auth: null
        }, 
        events: [
            {
                listen: 'test',
                scripts: {
                    type: 'text/javascript',
                    exec: requestTest
                }
            }
        ]
    })
} 

module.exports = { requests }
