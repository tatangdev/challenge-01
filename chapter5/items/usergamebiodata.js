const { Item } = require ('postman-collection');
const names = require('../names');
const endpoints = require('../endpoints');
const payloads = require('../payloads');
const headers = require('../headers');

// test if 200 then we can get the message of test successful kalau menggunakan jaz.
const requestTest = `
pm.test('Contoh Testing: Test for successfull response', function() {
    pm.expect(pm.response.code).to.equal(200);
});
`
const requests = { 
    create : new Item({
        name: names.userGameBiodata.create,
        request: {
            header: headers.auth.requestHeader.createUserGameBiodata,
            url: endpoints.userGameBiodata.create,
            method: 'GET',
            body: {
                mode: 'raw',
                raw: JSON.stringify(payloads.userGameBiodata.create)
            },
            auth: null
        }, 
        events: [
            {
                listen: 'test',
                scripts: {
                    type: 'text/javascript',
                    exec: requestTest
                }
            }
        ]
    }),

    update: new Item({
        name: names.userGameBiodata.update,
        request: {
            header: headers.auth.requestHeader.updateUserGameBiodata,
            url: endpoints.userGameBiodata.update,
            method: 'PUT',
            body: {
                mode: 'raw',
                raw: JSON.stringify(payloads.userGameBiodata.update)
            },
            auth: null
        }, 
        events: [
            {
                listen: 'test',
                scripts: {
                    type: 'text/javascript',
                    exec: requestTest
                }
            }
        ]
    }),

    show: new Item({
        name: names.userGameBiodata.show,
        request: {
            header: headers.auth.requestHeader.showUserGameBiodata,
            url: endpoints.userGameBiodata.show,
            method: 'GET',
            auth: null
        }, 
        events: [
            {
                listen: 'test',
                scripts: {
                    type: 'text/javascript',
                    exec: requestTest
                }
            }
        ]
    }),

    destroy: new Item({
        name: names.userGameBiodata.destroy,
        request: {
            header: headers.auth.requestHeader.destroyUserGameBiodata,
            url: endpoints.userGameBiodata.destroy,
            method: 'DELETE',
            auth: null
        }, 
        events: [
            {
                listen: 'test',
                scripts: {
                    type: 'text/javascript',
                    exec: requestTest
                }
            }
        ]
    }),
} 

module.exports = { requests }
