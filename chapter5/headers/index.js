const auth = require('./auth')
const noAuth = require('./noauth')

module.exports = { auth, noAuth }

