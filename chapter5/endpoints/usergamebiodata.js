require('dotenv').config()

const { 
    HOST
} = process.env

console.log(HOST);

module.exports = {
    create: `${HOST}/user-game-biodata/create`,
    update: `${HOST}/user-game-biodata/update`,
    show: `${HOST}/user-game-biodata/get-details`,
    destroy: `${HOST}/user-game-biodata/delete`
}

