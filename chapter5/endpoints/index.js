const auth = require('./auth')
const userGame = require('./usergame') 
const userGameBiodata = require('./usergamebiodata') 
const userGameHistory = require('./usergamehistory') 

module.exports = { auth, userGame, userGameBiodata, userGameHistory }
