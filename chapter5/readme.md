## Install the Package

To install the package we can try to create using `postman-collection` package.

```bash
yarn install
```

## To run the script

```bash
yarn run write 
```

To test the collection/collection.json you can use the postman or the newma.

## Convention 

Every names request is in the `./names/`  <br>
Every headers request is in the `./headers/`  <br>
Every endpoints request is in the `./endpoints/`  <br>
Every payloads request is in the `./payloads/`  <br>
Every items request is in the `./items/` <br>

Collection is saved in `./collection/collection.json`

