# Challenge - 01 Binar

Before we run the challenge we can try to install the script, just put.

``` 
yarn install 
```

Or if you prefer using npm just use

```
npm install
```

To run the challenge you should type 

```
yarn run count
```

Or

```
npm run count
```

Project Structures of this challenge is

```
challenge-01
├── chapter1
│   └── challenge
│       ├── index.js -- the main function (just importing the ./src/app.js )
│       ├── package.json -- scripts and packages
│       ├── src
│       │   ├── app.js -- the program 
│       │   ├── area.js -- the area function
│       │   ├── arithmetic.js -- the arithmetic function
│       │   ├── string.js -- to hold the string in a variable
│       │   └── volume.js -- the volume function
│       └── yarn.lock
└── readme.md
```

GEDE RICO WIJAYA - BE JS 1
