// This file is for calculating the arithmetic such as +, -, *, /, **, root of a number 
import readline from 'readline'
import string from './string.js'

const arithmetic = {

  // function untuk menghitung tabung
  addition : async () => {
    try {
      // create the interface for stdin and stdout
      const readLine = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
        terminal: false
      });

      // read the radius first then read the height
      readLine.question(string.enterFirstNumberAddition, firstNumber => {
        readLine.question(string.enterSecondNumberAddition, secondNumber => {
          const result = +firstNumber + +secondNumber;
          console.log(`${string.resultAddition} ${result} m2`);
        });
      });
    } catch(error) {
      throw error;
    }
  },

  reduction : () => {
    try {
      // create the interface for stdin and stdout
      const readLine = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
        terminal: false
      });

      // read the radius first then read the height
      readLine.question(string.enterSecondNumberReduction, firstNumber => {
        readLine.question(string.enterSecondNumberReduction, secondNumber => {
          const result = +firstNumber - +secondNumber;
          console.log(`${string.resultReduction} ${result}`);
        });
      });
    } catch(error) {
      throw error;
    }
  },
  

  multiplication : () => {
    try {
      // create the interface for stdin and stdout
      const readLine = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
        terminal: false
      });

      // read the radius first then read the height
      readLine.question(string.enterFirstNumberMultiplication, firstNumber => {
        readLine.question(string.enterSecondNumberMultiplication, secondNumber => {
          const result = +firstNumber * +secondNumber;
          console.log(`${string.resultMultiplication} ${result}`);
        });
      });
    } catch(error) {
      throw error;
    }
  },

  division : () => {
    try {
      // create the interface for stdin and stdout
      const readLine = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
        terminal: false
      });

      // read the radius first then read the height
      readLine.question(string.enterFirstNumberDivision, firstNumber => {
        readLine.question(string.enterSecondNumberDivision, secondNumber => {
          const result = +firstNumber / +secondNumber; // using unary operator to convert string into integer
          console.log(`${string.resultDivision} ${result}`);
        });
      });
    } catch(error) {
      throw error;
    }
  },

  power : () => {
    try {
      // create the interface for stdin and stdout
      const readLine = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
        terminal: false
      });

      // read the radius first then read the height
      readLine.question(string.enterFirstNumberPower, firstNumber => {
        readLine.question(string.enterSecondNumberPower, secondNumber => {
          const result = ((+firstNumber) ** (+secondNumber)); // using unary operator to convert string into integer
          console.log(`${string.resultPower} ${result}`);
        });
      });
    } catch(error) {
      throw error;
    }
  },

  root : () => {
    try {
      // create the interface for stdin and stdout
      const readLine = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
        terminal: false
      });

      // read the radius first then read the height
      readLine.question(string.enterFirstNumberRoot, number => {
        const result = Math.sqrt(number); // using unary operator to convert string into integer
        console.log(`${string.resultRoot} ${result}`);
      });
    } catch(error) {
      throw error;
    }
  },
}
  
export default arithmetic;
