import readline from 'readline'
import string from './string.js'
import area from './area.js'
import app from './app.js'

const volume = {
  // function untuk menghitung kubus
  cube: async () => {
    try {
      const readLine = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
        terminal: false
      });

      readLine.question(string.enterNumberCube, side => {

        // count the cube
        console.log(`${string.resultCube} ${side * side * side} m3`);
        // close the readLine
        app.run()
      });
    } catch (error) {
      throw error;
    }
  },

  // function untuk menghitung tabung
  cylinder : async () => {
    try {
      // create the interface for stdin and stdout
      const readLine = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
        terminal: false
      });

      // read the radius first then read the height
      readLine.question(string.enterRadius, radius => {
        readLine.question(string.enterHeight, height => {
          console.log(`${string.resultCylinder} ${area.circle(radius) * height} m2`)
        });
      });
    } catch(error) {
      throw error;
    }
  }
}

export default volume;


