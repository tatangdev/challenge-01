class Student {
  constructor(grade) { 
    this._grade = grade;
  }

  getGrade = () => {
    return this._grade;
  }
}

export default Student;
