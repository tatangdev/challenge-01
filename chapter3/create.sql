
-- one publisher can have many books

CREATE TABLE publishers(
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(50),
  address TEXT,
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW()
); 

-- one books can be had by many publisher

CREATE TABLE books(
  id BIGSERIAL PRIMARY KEY,
  publisher_id INTEGER,
  name VARCHAR(50),
  writer VARCHAR(50),
  release_date DATE,
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW(),
  FOREIGN KEY (publisher_id) REFERENCES publishers(id)
); 

-- users and profile (one on one)

CREATE TABLE users(
  id BIGSERIAL PRIMARY KEY,
  username VARCHAR(12) NOT NULL UNIQUE,
  password VARCHAR(12) NOT NULL,
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW()
); 

CREATE TABLE profiles(
  id BIGSERIAL PRIMARY KEY,
  fullname VARCHAR(50) NOT NULL,
  address TEXT,
  profile_id INTEGER,
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW(),
  FOREIGN KEY (profile_id) REFERENCES users(id)
);

CREATE TABLE book_stocks (
  book_id INTEGER,
  stock INTEGER,
  FOREIGN KEY (book_id) REFERENCES books(id)
);

CREATE TABLE loan_transaction (
  id BIGSERIAL PRIMARY KEY,
  profile_id INTEGER,
  book_id INTEGER,
  amount_of_book INTEGER DEFAULT 1,
  loan_date DATE DEFAULT NOW() NOT NULL,
  return_date DATE,
  FOREIGN KEY(profile_id) REFERENCES profiles(id),
  FOREIGN KEY(book_id) REFERENCES books(id)
);
